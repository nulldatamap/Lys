﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;

namespace Lys {

    public class UndefinedSymbol : Exception {
        public Symbol Symbol { get; private set; }

        public UndefinedSymbol( Symbol s ) : base( $"Undefined symbol: `{s.Name}`" ) {
            Symbol = s;
        }
    }

    public class UncallableValue : Exception {
        public object Callee { get; private set; }

        public UncallableValue( object v ) {
            Callee = v;
        }
    }

    public class InvalidArguments : Exception {
        public object[] Values { get; private set; }
        public MethodBase Method { get; private set; }

        public InvalidArguments( string msg ) : base( msg ) {
        }
    }

    public class InvalidForm : Exception {
        public InvalidForm( string msg ) : base( msg ) { }
    }

    public class TypeError : Exception {
        public object Value { get; private set; }
        public Type Expected { get; private set; }

        public TypeError( object o, Type t ) {
            Value = o;
            Expected = t;
        }
    }

    public abstract class AmbiguousMember {
        public String Name { get; internal set; }
        public Type Class { get; internal set; }

        public AmbiguousMember( Type cls, string name ) {
            Name = name;
            Class = cls;
        }

        public abstract MemberInfo Disambiguate( object[] arguments );

        public static MethodBase ResolveInvoke( IEnumerable<MethodBase> methods
                                              , IEnumerable<object> args ) {
            return ResolveInvoke( methods, args.Select( arg => arg == null ? null : arg.GetType() )
                                               .ToArray() );
        }

        public static MethodBase ResolveInvoke( IEnumerable<MethodBase> methods, Type[] types ) {
            // This method goes through and tries to find the most specific overload of the given
            // method based on the types of the paremeters.

            // The way we resolve overloads is by ranking each parameter type's "depth" compared
            // to the method's parameters type. The depths is how far down the class hierachy the
            // input type is. The method with the smallest unique depth is considered the most 
            // specific.

            var shallowestDepth = Int32.MaxValue;
            var typeDepths = methods.Select( meth => {
                var parameters = meth.GetParameters();
                var depths = new int[parameters.Length];

                for( int i = 0; i < parameters.Length; i++ ) {
                    // If the overload has more parameters than supplied, if they're optional then
                    // it's still a valid candidate
                    if( i >= types.Length ) {
                        if( parameters[i].IsOptional ) break;
                        else return null;
                    }

                    var paramType = parameters[i].ParameterType;
                    int currentDepth = 0;
                    var currentType = types[i];

                    // If the input parameter was null and the method takes a struct, then it's not
                    // applicable, otherwise it's a class it's considered to be a match
                    if( currentType == null ) {
                        if( paramType.IsValueType ) return null;
                        else {
                            depths[i] = 0;
                            continue;
                        }
                    }

                    // Go Through until we either are sure that the types don't match or we've 
                    // found the depth
                    while( currentType != paramType && currentType != null ) {
                        currentType = currentType.BaseType;
                        currentDepth++;
                    }

                    if( currentType == paramType ) {
                        depths[i] = currentDepth;
                        if( currentDepth < shallowestDepth ) shallowestDepth = currentDepth;
                    }  else return null;
                }

                return new Tuple<int[], MethodBase>( depths, meth );

            } ).Where( depths => depths != null )
               .ToArray();

            var candiates = typeDepths;
            var secondShallowest = Int32.MaxValue;
            var runDry = false;

            // Keep refining candiates until there are no ambiguities or we're sure the call is
            // truely ambiguous.
            while( candiates.Count() > 1 && !runDry ) {
                // Go though all the depths and recond how many match the current shallowest depth
                var counts = candiates.Select( cand => {
                    var depths = cand.Item1;
                    var count = 0;
                    foreach( var depth in depths ) {
                        if( depth == shallowestDepth ) count++;
                        // Record the second shallowest depth too for next iteration
                        if( depth > shallowestDepth && depth < secondShallowest )
                            secondShallowest = depth;
                    }

                    // If none was found make sure that if no match is found this iteration that
                    // it's considered ambiguous.
                    if( secondShallowest == Int32.MaxValue ) runDry = true;

                    return count;
                } );
                
                var highestCount = counts.Max();
                // Pick all the candidates who have the highest amount of shallowest matches
                var newCandidates = counts.Zip( candiates, (count, cand) => {
                    if( count != highestCount ) return null;
                    return cand;
                } ).Where( ncand => ncand != null )
                   .ToArray();

                shallowestDepth = secondShallowest;
                secondShallowest = Int32.MaxValue;
                candiates = newCandidates;
            }

            var methCount = candiates.Count();
            // If there's no proper match or it's ambiguous return null
            if( methCount < 1 )
                throw new InvalidArguments( $"Provided arguments match none of the method " +
                                            $"overloads" );
            else if( methCount > 1 )
                throw new InvalidArguments( $"The provided arguments result in an ambiguous " +
                                            $"call" );

            return candiates.FirstOrDefault().Item2;
        }
    }

    public class AmbiguousConstructor : AmbiguousMember {

        public AmbiguousConstructor( Type cls ) : base( cls, cls.Name ) { }

        public override MemberInfo Disambiguate( object[] arguments ) {
            return ResolveInvoke( Class.GetConstructors(), arguments );
        }
    }

    public class AmbiguousInstanceMember : AmbiguousMember {

        public AmbiguousInstanceMember( Type cls, string memberName ) : base( cls, memberName ) { }

        public override MemberInfo Disambiguate( object[] arguments ) {
            if( arguments.Length < 1 )
                throw new InvalidArguments( $"Can't invoke an instance member `{Name}` without" +
                                            $" an instance" );

            var instance = arguments[0];

            if( instance == null )
                throw new InvalidArguments( $"Can't invoke an instance member `{Name}` on " +
                                            $"`null`" );

            var instanceType = Class ?? instance.GetType();
            var memberCandidates =
                instanceType.GetMembers( BindingFlags.Public | BindingFlags.Instance )
                            .Where( mem => mem.Name == Name )
                            .ToArray();

            if( memberCandidates.Length == 0 )
                throw new InvalidArguments( $"No member `{Name}` for type `{instanceType.Name}`" );

            if( memberCandidates.Length > 1 ) {
                if( !memberCandidates.All( mem => mem.MemberType == MemberTypes.Method ) )
                    throw new InvalidArguments( $"Encountered multiple members named `{Name}` in" +
                                                $" `{instanceType.Name}` that are not methods" );
                // Skip the instance argument
                return ResolveInvoke( memberCandidates.Cast<MethodBase>(), arguments.Skip( 1 ) );
            }

            return memberCandidates[0];
        }
    }

    public class AmbiguousStaticMember : AmbiguousMember {

        public AmbiguousStaticMember( Type cls, string memberName ) : base( cls, memberName ) { }

        public override MemberInfo Disambiguate( object[] arguments ) {
            var memberCandidates =
                Class.GetMembers( BindingFlags.Public | BindingFlags.Static )
                     .Where( mem => mem.Name == Name )
                     .ToArray();

            if( memberCandidates.Length == 0 )
                throw new InvalidArguments( $"No static member `Name` for type `{Class.Name}`" );

            if( memberCandidates.Length > 1 ) {
                if( !memberCandidates.All( mem => mem.MemberType == MemberTypes.Method ) )
                    throw new InvalidArguments( $"Encountered multiple statuc members named " +
                                                $"`{Name}` in `{Class.Name}` that are not" +
                                                $"methods" );

                return ResolveInvoke( memberCandidates.Cast<MethodBase>(), arguments );
            }

            return memberCandidates[0];
        }
    }


    public static class Constants {
        public static readonly Symbol SYM_DEFUN = new Symbol( null, "defun" );
        public static readonly Symbol SYM_LET = new Symbol( null, "let" );
        public static readonly Symbol SYM_UNQUOTE = new Symbol( null, "unquote" );
        public static readonly Symbol SYM_UNQUOTE_SPLICE = new Symbol( null, "unquote-splice" );
        public static readonly Symbol SYM_CATCH = new Symbol( null, "catch" );
        public static readonly Symbol SYM_FINALLY = new Symbol( null, "finally" );


        public static readonly Keyword KW_STATIC = new Keyword( "static" );
    }

    public class Interpreter {
        public static int Add( int x, int y ) {
            return x + y;
        }

        public static int Sub( int x, int y ) {
            return x - y;
        }

        public static int Mul( int x, int y ) {
            return x * y;
        }

        public static int Div( int x, int y ) {
            return x / y;
        }

        public static int Mod( int x, int y ) {
            return x % y;
        }

        public static bool And( bool x, bool y ) {
            return x && y;
        }

        public static bool Or( bool x, bool y ) {
            return x || y;
        }

        public static bool Not( bool x ) {
            return !x;
        }

        public static bool LessThan( int x, int y ) {
            return x < y;
        }

        public static bool GreaterThan( int x, int y ) {
            return x > y;
        }

        public static bool LessThanOrEqual( int x, int y ) {
            return x <= y;
        }

        public static bool GreaterThanOrEqual( int x, int y ) {
            return x >= y;
        }

        public static bool Is( object x, Type t ) {
            return x.GetType() == t;
        }

        Dictionary<string, Delegate> macros = new Dictionary<string, Delegate>();
        Stack<List<Type>> importScope = new Stack<List<Type>>();
        Stack<Dictionary<string, object>> scope = new Stack<Dictionary<string, object>>();
        Stack<Cons> callStack = new Stack<Cons>();

        public Interpreter() {
            scope.Push( new Dictionary<string, object> {
                { "print", (Action<String>) (x => Console.WriteLine( x )) },
                { "i+", (Func<int, int, int>) Add },
                { "i-", (Func<int, int, int>) Sub },
                { "i*", (Func<int, int, int>) Mul },
                { "idiv", (Func<int, int, int>) Div },
                { "i%", (Func<int, int, int>) Mod },
                { "b&&", (Func<bool, bool, bool>) And },
                { "b||", (Func<bool, bool, bool>) Or },
                { "b!", (Func<bool, bool>) Not },
                { "i<", (Func<int, int, bool>) LessThan },
                { "i>", (Func<int, int, bool>) GreaterThan },
                { "i<=", (Func<int, int, bool>) LessThanOrEqual },
                { "i>=", (Func<int, int, bool>) GreaterThanOrEqual },
                { "=", (Func<object, object, bool>) Object.Equals },
                { "is", (Func<object, Type, bool>) Is },
                { "%show-scope", (Action) ( () => { ShowScope(); } ) },
                { "%show-imports", (Action) ( () => { ShowImports(); } ) },
                { "%get-macro", (Func<string, Delegate>) ((m) => macros[m] )},
                { "%expand-macro", (Func<object, object>)( (form) => ExpandMacro( form ) ) }
            } );

            importScope.Push( new List<Type>() );
        }

        /// <summary>
        /// Reset the context and evaluate an expression.
        /// </summary>
        /// <remarks>The reason why you would want to reset the context, is that
        /// when an exception is thrown, the call stack and local scopes are left
        /// in place for debugging purposes. </remarks>
        public object Do( object expr ) {
            // Reset the callstack
            callStack = new Stack<Cons>();
            // Make sure our scope is only top-level
            while( scope.Count > 1 ) scope.Pop();
            return Eval( expr );
        }

        public object EvalEach( IEnumerable<object> exprs ) {
            object result = null;
            foreach( var expr in exprs ) {
                result = Eval( expr );
            }
            return result;
        }

        public object ExpandMacro( object expr ) {
            var sexp = expr as Cons;
            if( sexp == null ) return expr;

            var head = sexp.Head as Symbol?;

            if( head == null ) return expr;
            if( macros.ContainsKey( head.Value.Name ) ) {
                var macroArgs = sexp.Skip( 1 )
                                    .Select( arg => ExpandMacro( arg ) )
                                    .ToArray();
                var macro = macros[head.Value.Name];
                expr = macro.DynamicInvoke( macroArgs );
            } else {
                if( head.Value.Name != "quote" ) {
                    var current = sexp.Tail;
                    while( current != null ) {
                        current.Head = ExpandMacro( current.Head );
                        current = current.Tail;
                    }
                }
            }

            return expr;
        }


        public object Eval( object expr ) {
            expr = ExpandMacro( expr );

            var sexp = expr as Cons;
            if( sexp != null ) {
                return EvalSexp( sexp );
            }

            var sym = expr as Symbol?;
            if( sym != null ) {
                return LookupSymbol( sym.Value );
            }

            return expr;
        }

        public object EvalSexp( Cons sexp ) {
            // () => ()
            if( sexp == null ) return sexp;
            callStack.Push( sexp );
            var car = sexp.Head;

            object specialFormResult;
            if( EvalSpecialForm( sexp, out specialFormResult ) ) return specialFormResult;

            var callee = Eval( car );

            if( IsCallable( callee ) ) {
                var args = sexp.Skip( 1 )
                                .Select( arg => Eval( arg ) )
                                .ToArray();

                var amb = callee as AmbiguousMember;
                var ctor = callee as Type;

                if( amb == null && ctor != null )
                    amb = new AmbiguousConstructor( ctor );

                MemberInfo member = null;
                if( amb != null ) {
                    callee = member = amb.Disambiguate( args );
                }

                if( member != null && member.MemberType == MemberTypes.Field ) {
                    var field = member as FieldInfo;
                    
                    return field.GetValue( field.IsStatic ?  null : args[0] );
                }

                if( member != null && member.MemberType == MemberTypes.Property ) {
                    var proper = member as PropertyInfo;
                    if( !proper.CanRead )
                        throw new InvalidArguments( $"Can't read property `{proper.Name}` from" +
                                                    $" `{proper.DeclaringType.Name}`" );
                    var isStatic = proper.GetMethod.IsStatic;

                    return proper.GetValue( isStatic ? null : args[0] );
                }

                var inf = callee as MethodBase;

                if( inf == null ) {
                    var calleeType = callee.GetType();
                    inf = calleeType.GetMethod( "Invoke" )
                            ?? calleeType.GetMethod( "DynamicInvoke" );
                    // Add the lambda to the call is the `this`
                    var nargs = new object[args.Length + 1];
                    nargs[0] = callee;
                    Array.Copy( args, 0, nargs, 1, args.Length );
                    args = nargs;
                    ValidateArgs( inf, args );
                }

                object subject = null;
                object[] effArgs = args;
                if( !inf.IsStatic && !inf.IsConstructor ) {
                    subject = args[0];
                    effArgs = args.Skip( 1 ).ToArray();
                } else if( inf.IsConstructor ) {
                    subject = System.Runtime
                                    .Serialization
                                    .FormatterServices
                                    .GetUninitializedObject( inf.DeclaringType );
                }

                object result;

                try {
                    result = inf.Invoke( subject, effArgs );
                } catch( TargetInvocationException e ) {
                    throw e.InnerException;
                }

                callStack.Pop();
                if( inf.IsConstructor ) return subject;
                return result;
            } else {
                throw new UncallableValue( callee );
            }
        }
        
        public bool ValidateArgs( MethodBase m, object[] args, string alias = null ) {
            var argn = 0;
            var expected = args.Length;

            if( !m.IsStatic && !m.IsConstructor ) {
                argn = 1;
                expected -= 1;

                if( args.Length == 0 )
                    throw new InvalidArguments( $"Can't access member without instance" );

                if( args[0] == null )
                    throw new InvalidArguments( $"Can't access member on `null`." );

                if( !m.DeclaringType.IsAssignableFrom( args[0].GetType() ) )
                    throw new InvalidArguments( $"Invalid instance type: `{m.Name}` acts on " +
                                                $"`{m.DeclaringType.Name}`, but was called with " +
                                                $"`{args[0].GetType().Name}`" );
            }

            foreach( var p in m.GetParameters() ) {
                if( argn >= args.Length )
                    throw new InvalidArguments( $"Too few arguments: { m.Name} takes {argn}, got" +
                                                $" {args.Length}" );

                if( p.IsIn || p.IsOptional || p.IsOut || p.IsRetval )
                    throw new NotImplementedException( $"Unimplmented method paremeter for: {p}" );

                if( args[argn] != null
                 && !p.ParameterType.IsAssignableFrom( args[argn].GetType() ) )
                    throw new InvalidArguments( $"Invalid argument type: `{m.Name}` expected" +
                                                $"argument `{p.Name}` (#{argn + 1}) to be " +
                                                $"`{p.ParameterType.Name}`, got " +
                                                $"`{args[argn].GetType().Name}`" );

                argn++;
            }
            if( argn != args.Length )
                throw new InvalidArguments( $"Too many arguments: `{m.Name}` takes {argn}, got " +
                                            $"{args.Length}" );

            return true;
        }

        public bool IsCallable( object v ) {
            if( v is Type ) return true;
            if( v is AmbiguousMember ) return true;
            if( v is MethodBase ) return true;
            return v.GetType().GetMethod( "Invoke" ) != null;
        }

        public string ConvertOperatorNames( string op ) {
            switch( op ) {
            case "==": return "op_Equality";
            case "!=": return "op_Inequality";
            case "+": return "op_Addition";
            case "-": return "op_Subtraction";
            case "<": return "op_LessThan";
            case ">": return "op_GreaterThan";
            case "<=": return "op_LessThanOrEqual";
            case ">=": return "op_GreaterThanOrEqual";
            default: return op;
            }
        }


        public object LookupSymbol( Symbol s ) {
            foreach( var currentScope in scope ) {
                object val;
                if( currentScope.TryGetValue( s.Name, out val ) ) {
                    return val;
                }
            }

            var qualifiedName = s.Name;
            if( s.Namespace != null )
                qualifiedName = s.Namespace.Replace( "::", "." ) + "." + qualifiedName;

            var cls = Type.GetType( qualifiedName, false );

            if( cls == null && s.Namespace == null ) {
                cls = TypeFromScope( s.Name );
            }

            if( cls != null ) return cls;

            throw new UndefinedSymbol( s );
        }

        public bool EvalSpecialForm( Cons sexp, out object result ) {
            result = null;
            var msym = sexp.Head as Symbol?;

            if( msym == null ) return false;
            var sym = msym.Value;

            switch( sym.Name ) {
            case ".":
                result = EvalDot( sexp );
                break;
            case "quote":
                result = EvalQuote( sexp );
                break;
            case "defun":
                result = EvalDefun( sexp );
                break;
            case "let":
                result = EvalLet( sexp );
                break;
            case "if":
                result = EvalIf( sexp );
                break;
            case "lambda":
                result = EvalLambda( sexp );
                break;
            case "macrofun":
                result = EvalMacrofun( sexp );
                break;
            case "unquote":
                throw new InvalidForm( "`unquote` is not allowed outside of a `quasiquote`" );
            case "unquote-splice":
                throw new InvalidForm( "`unquote-splice` is not allowed outsite of a " +
                                       "`quasiquote`" );
            case "quasiquote":
                result = EvalQuasiQuote( sexp );
                break;
            case "do":
                result = EvalDo( sexp );
                break;
            case "using":
                EvalUsing( sexp );
                break;
            case "foreach":
                result = EvalForeach( sexp );
                break;
            case "try":
                result = EvalTry( sexp );
                break;
            case "throw":
                EvalThrow( sexp );
                break;
            default:
                return false;
            }

            return true;
        }

        public object EvalDot( Cons sexp ) {
            if( sexp.Tail == null )
                throw new InvalidForm( $"`.` expects at least one field argument" );

            Symbol field;
            
            Symbol? subject = null;
            bool isStatic = false;

            if( sexp.Tail.Tail != null ) {
                if( sexp.Tail.Head != null )
                    subject = AssertSType<Symbol>( sexp.Tail.Head );

                field = AssertSType<Symbol>( sexp.Tail.Tail.Head );

                if( sexp.Tail.Tail.Tail != null ) {
                    if( sexp.Tail.Tail.Tail.Head as Keyword? != Constants.KW_STATIC )
                        throw new InvalidForm( $"`.` only accepts `:static` as its third " +
                                               $"argument" );

                    isStatic = true;

                    if( sexp.Tail.Tail.Tail.Tail != null )
                        throw new InvalidForm( $"`.` at most expects 3 arguments, got " +
                                               $"{sexp.Count() - 1} arguments" );
                }

            } else {
                field = AssertSType<Symbol>( sexp.Tail.Head );
            }

            if( field.Namespace != null )
                throw new InvalidForm( $"`.` only takes fields without a namespace" );

            if( subject == null && !isStatic ) {
                return new AmbiguousInstanceMember( null, field.Name );
            }

            if( subject == null && isStatic )
                throw new InvalidForm( "Unqualified static members are not allowed" );

            var cls = AssertType<Type>( LookupSymbol( subject.Value ) );

            // It's a constructor:
            if( field == null ) {
                return cls;
            }

            // Convert operators to their method names
            var fieldStr = ConvertOperatorNames( field.Name );

            if( isStatic )
                return new AmbiguousStaticMember( cls, fieldStr );
            else
                return new AmbiguousInstanceMember( cls, fieldStr );
        }

        public object EvalQuote( Cons sexp ) {
            if( sexp.Tail == null ) return null;
            if( sexp.Tail.Tail != null )
                throw new InvalidForm( $"`quote` expects 0 to 1 arguments, got " +
                                       $"{sexp.Count() - 1}" );

            return sexp.Tail.Head;
        }

        public object EvalDefun( Cons sexp ) {
            if( sexp.ShorterThan( 3 ) )
                throw new InvalidForm( $"`defun` expects at least a name and function " +
                                       $"arguments, only got {sexp.Count() - 1} arguments" );

            var name = AssertSType<Symbol>( sexp.Tail.Head );
            var args = AssertType<List<object>>( sexp.Tail.Tail.Head )
                .Select( arg => AssertSType<Symbol>( arg ) );
            var body = sexp.Skip( 3 );

            scope.Peek()[name.Name] = MakeFunc( name.Name, args, body );
            return name;
        }

        public void EnterFunc( string funcName ) {
            // TODO:
            scope.Push( new Dictionary<string, object>() );
            importScope.Push( new List<Type>() );
        }

        public void LeaveFunc() {
            // TODO:
            scope.Pop();
            importScope.Pop();
        }

        public void ShowScope() {
            var depth = 0;
            foreach( var subscope in scope ) {
                Console.Write( $"[{depth}]\t" );
                var first = true;
                foreach( var kv in subscope ) {
                    if( !first ) Console.Write( "\t" );
                    Console.WriteLine( $"{kv.Key}:\t\t{Lisp.ShowLisp( kv.Value )}" );
                    first = false;
                }
                depth++;
            }
        }

        public void ShowImports() {
            var depth = 0;
            foreach( var subscope in importScope ) {
                Console.Write( $"[{depth}]\t" );
                var first = true;
                foreach( var ty in subscope ) {
                    if( !first ) Console.Write( "\t" );
                    Console.WriteLine( $"{ty}" );
                    first = false;
                }
                depth++;
            }
        }

        public object EvalLet( Cons sexp ) {
            if( sexp.ShorterThan( 2 ) )
                throw new InvalidForm( $"`let` expects at least bindings definitions, only" +
                                       $" got {sexp.Count() - 1} arguments" );

            var bindings = AssertType<List<object>>( sexp.Tail.Head );
            var body = sexp.Skip( 2 );

            if( bindings.Count % 2 != 0 )
                throw new InvalidForm( $"Bindings must be symbol-value pairs, got uneven " +
                                       $"number of values" );

            object result = null;
            var currentScope = new Dictionary<string, object>();
            scope.Push( currentScope );

            var bindingsEnumer = bindings.GetEnumerator();
            while( bindingsEnumer.MoveNext() ) {
                var sym = AssertSType<Symbol>( bindingsEnumer.Current );
                bindingsEnumer.MoveNext();
                currentScope[sym.Name] = Eval( bindingsEnumer.Current );
            }

            foreach( var expr in body ) {
                result = Eval( expr );
            }

            scope.Pop();
            return result;
        }

        public object EvalLambda( Cons sexp ) {
            if( sexp.ShorterThan( 2 ) )
                throw new InvalidForm( $"`lambda` expects at least lambda arguments, got " +
                                       $"only {sexp.Count() - 1} arguments" );

            var args = AssertType<List<object>>( sexp.Tail.Head )
                .Select( arg => AssertSType<Symbol>( arg ) );
            var body = sexp.Skip( 2 );
            return MakeFunc( $"%__lambda{sexp.GetHashCode()}", args, body );
        }

        public object EvalIf( Cons sexp ) {
            if( sexp.ShorterThan( 3 ) )
                throw new InvalidForm( $"`if` expects at least a condition and a then-body, " +
                                       $"got {sexp.Count() - 1} arguments" );

            var cond = sexp.Tail.Head;
            var then = sexp.Tail.Tail.Head;
            object els = null;
            if( sexp.Tail.Tail.Tail != null ) {
                els = sexp.Tail.Tail.Tail.Head;
                if( sexp.Tail.Tail.Tail.Tail != null )
                    throw new InvalidForm( $"`if` expects at most a condition, a then-body and" +
                                           $"an else-body, got {sexp.Count() - 1} arguments" );
            }

            object result = els;

            var condResult = Eval( cond );

            if( condResult != null && !condResult.Equals( false ) ) result = then;

            return Eval( result );
        }

        public object EvalMacrofun( Cons sexp ) {
            if( sexp.ShorterThan( 3 ) )
                throw new InvalidForm( $"`macrofun` expects at least a marcro name and macro" +
                                       $"arguments, got {sexp.Count() - 1} arguments" );

            var name = AssertSType<Symbol>( sexp.Tail.Head ).Name;
            var args = AssertType<List<object>>( sexp.Tail.Tail.Head )
                .Select( x => AssertSType<Symbol>( x ) );
            var body = sexp.Skip( 3 );

            macros[name] = MakeFunc( name, args, body );

            return null;
        }

        public object EvalQuasiQuote( Cons sexp ) {
            if( sexp.Tail == null ) return null;
            if( sexp.Tail.Tail != null )
                throw new InvalidForm( $"`quasiquote` expects 0 to 1arguments, got" +
                                       $"{sexp.Count() - 1}" );

            return EvalUnquotes( sexp.Tail.Head );
        }

        public object EvalUnquotes( object obj ) {
            var sexp = obj as Cons;
            if( sexp == null ) return obj;

            var current = sexp;
            while( current != null ) {
                var val = current.Head;

                var msexp = val as Cons;
                if( msexp != null ) {
                    if( Constants.SYM_UNQUOTE.Equals( msexp.Head ) ) {
                        current.Head = Eval( msexp.Tail.Head );
                        continue;
                    } else if( Constants.SYM_UNQUOTE_SPLICE.Equals( msexp.Head ) ) {
                        var spliceVal = AssertType<Cons>( Eval( msexp.Tail.Head ) );
                        current.Head = spliceVal.Head;
                        var nextup = current.Tail;
                        current.Tail = spliceVal.Tail;
                        // Replace the last element of the spliced tail with the current tail
                        if( nextup != null ) {
                            var spliceTail = spliceVal.Tail;
                            while( spliceTail.Tail != null ) spliceTail = spliceTail.Tail;
                            spliceTail.Tail = nextup;
                        }
                        current = nextup;
                        continue;
                    }
                }

                current.Head = EvalUnquotes( val );

                current = current.Tail;
            }

            return sexp;
        }

        public object EvalDo( Cons sexp ) {
            if( sexp.Tail == null ) return null;
            object result = null;

            foreach( var expr in sexp.Tail ) {
                result = Eval( expr );
            }

            return result;
        }

        public void EvalUsing( Cons sexp ) {
            if( sexp.Tail == null )
                throw new InvalidForm( $"`using` takes at least one namespace to import" );

            var names =
                sexp.Tail.Select( ns => AssertSType<Symbol>( ns ).ToCSharpQualifiedName() );

            var importedTypes =
                AppDomain.CurrentDomain.GetAssemblies()
                                       .SelectMany( x => x.GetTypes() )
                                       .Where( ty => names.Contains( ty.Namespace ) );
            var currentScope = importScope.Peek();
            bool importedAny = false;

            foreach( var importedType in importedTypes ) {
                if( !TypeInScope( importedType ) ) {
                    importedAny = true;
                    currentScope.Add( importedType );
                } // TOOD: else warn
            }

            if( !importedAny ) throw new Exception( $"Nothing in used namepsace" );
        }

        public object EvalForeach( Cons sexp ) {
            if( sexp.ShorterThan( 3 ) )
                throw new InvalidForm( $"`foreach` expects at least a loop variable symbol" +
                                       $"and loop value" );

            var variable = AssertSType<Symbol>( sexp.Tail.Head );
            var value =
                AssertType<System.Collections.IEnumerable>( Eval( sexp.Tail.Tail.Head ) );
            var body = sexp.Tail.Tail.Tail;

            if( value == null ) return null;

            var loopScope = new Dictionary<string, object>();
            scope.Push( loopScope );
            object result = null;
            foreach( var v in value ) {
                loopScope[variable.Name] = v;
                foreach( var exp in body ) {
                    result = Eval( exp );
                }
            }

            scope.Pop();
            return result;
        }

        
        public object EvalTry( Cons sexp ) {
            if( sexp.ShorterThan( 2 ) )
                throw new InvalidForm( $"`try` expects at least a body" );

            var body = sexp.Tail.Head;
            var catchClauses = sexp.Tail.Tail;

            object result = null;
            Exception exception = null;

            try {
                result = Eval( body );
            } catch( Exception e ) {
                exception = e;
            }

            if( catchClauses != null )
                foreach( var clause in catchClauses ) {
                    var catchClause = AssertType<Cons>( clause );
                    var clauseHead = AssertSType<Symbol>( catchClause.Head );

                    if( clauseHead == Constants.SYM_CATCH ) {
                        if( catchClause.ShorterThan( 3 ) )
                            throw new InvalidForm( $"`catch` takes at least an exception " +
                                                   $"type and exception variable" );

                        var exceptionName = AssertSType<Symbol>( catchClause.Tail.Head );
                        var exceptionType = AssertType<Type>( LookupSymbol( exceptionName ) );

                        if( exception == null
                         || !exceptionType.IsAssignableFrom( exception.GetType() ) )
                            continue;

                        var exceptionBinding = AssertSType<Symbol>( catchClause.Tail.Tail.Head );

                        var catchScope = new Dictionary<string, object> {
                            { exceptionBinding.Name, exception }
                        };

                        scope.Push( catchScope );
                        result = EvalEach( catchClause.Tail.Tail.Tail );
                        scope.Pop();
                        exception = null;

                    } else if( clauseHead == Constants.SYM_FINALLY ) {
                        result = EvalEach( catchClause.Tail );
                        break;
                    } else
                        throw new InvalidForm( "`try` only accepts `catch` of `finally` " +
                                               "clauses after the body" );
                }

            if( exception != null ) throw exception;
            return result;
        }

        public void EvalThrow( Cons sexp ) {
            if( sexp.Tail == null || sexp.Tail.Tail != null )
                throw new InvalidForm( $"`throw` expects exactly one argument, got " +
                                       $"{sexp.Count() - 1}" );

            var exception = AssertType<Exception>( Eval( sexp.Tail.Head ) );
            throw exception;
        }

        public bool TypeInScope( Type ty ) {
            foreach( var scope in importScope ) {
                if( scope.Contains( ty ) ) return true;
            }

            return false;
        }

        public Type TypeFromScope( string name ) {
            foreach( var scope in importScope ) {
                foreach( var ty in scope ) {
                    if( ty.Name == name ) return ty;
                }
            }

            return null;
        }

        public Delegate MakeFunc( string name, IEnumerable<Symbol> args
                                , IEnumerable<object> body ) {
            var thisType = typeof( Interpreter );
            var thisConst = Expression.Constant( this );

            var innerParams = args.Select( arg =>
                Expression.Parameter( typeof( object ), arg.Name ) ).ToArray();

            var newScope = Expression.Call( thisConst, thisType.GetMethod( "EnterFunc" )
                                          , Expression.Constant( name ) );

            var bindings =
                innerParams.Select( ( p ) => {
                    var scopeType = typeof( Stack<Dictionary<string, object>> );
                    var subscopeType = typeof( Dictionary<string, object> );
                    var peekCall =
                        Expression.Call( Expression.Constant( scope )
                                       , scopeType .GetMethod( "Peek" ) );
                    return Expression.Call( peekCall
                                          , subscopeType.GetMethod( "Add" )
                                          , Expression.Constant( p.Name )
                                          , p );
                } );

            var returnVar = Expression.Variable( typeof( object ) );
            var evalEachCall = Expression.Call( thisConst
                                              , thisType.GetMethod( "EvalEach" )
                                              , Expression.Constant( body ) );
            var evalExprs = Expression.Assign( returnVar, evalEachCall );
            var leave = Expression.Call( thisConst, thisType.GetMethod( "LeaveFunc" ) );
            var bodyExpr =
                Expression.Block( new[] { returnVar }
                                , new Expression[] { newScope }.Concat( bindings )
                                                               .Concat( new Expression[] {
                                                                   evalExprs, leave, returnVar
                                                               } ) );
            var innerFunc = Expression.Lambda( bodyExpr, innerParams );

            return innerFunc.Compile();
        }

        public T AssertType<T>( object v ) where T : class {
            if( v == null ) return null;
            var r = v as T;
            if( r == null ) throw new TypeError( v, typeof( T ) );
            return r;
        }

        public T AssertSType<T>( object v ) where T : struct {
            var r = v as T?;
            if( r == null ) throw new TypeError( v, typeof( T ) );
            return r.Value;
        }

        public string StackTrace() {
            int depth = 0;
            var str = new StringBuilder();
            foreach( var elm in callStack ) {
                str.AppendFormat( $"[{depth}] {Lisp.ShowLisp( elm )}\n" );
                depth++;
            }

            return str.ToString();
        }
    }
}
