﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;
using System.Collections;

namespace Lys {

    public class ConsEnumerator : IEnumerator<object>, IDisposable {
        private Cons head;
        private Cons current;
        private bool done;
        public object Current {
            get { return current.Head; }
        }

        public ConsEnumerator( Cons head ) {
            this.head = head;
            current = null;
            done = false;
        }

        public bool MoveNext() {
            if( !done && current == null ) {
                current = head;
                return true;
            }

            if( current.Tail != null ) {
                current = current.Tail;
                return true;
            } else {
                current = null;
                done = true;
                return false;
            }
        }

        public void Reset() {
            current = null;
            done = false;
        }

        public void Dispose() { }
    }

    public class Cons : IEnumerable<object> {
        public object Head {
            get;
            set;
        }

        public Cons Tail {
            get;
            set;
        }

        public Cons( object car, Cons cdr ) {
            Head = car;
            Tail = cdr;
        }

        public override bool Equals( object obj ) {
            var cons = obj as Cons;
            return cons != null
                && cons.Head.Equals( Head )
                && ( ( Tail == null && cons.Tail == null ) || cons.Tail.Equals( Tail ) );
        }

        public override string ToString() {
            var tail = Tail == null ? "null" : Tail.ToString();
            return $"(cons {Head} {tail})";
        }

        public int Count() {
            if( Tail == null ) return 1;
            return 1 + Tail.Count();
        }

        public bool ShorterThan( int x ) {
            if( x <= 1 ) return false;
            if( Tail == null ) return true;
            return Tail.ShorterThan( x - 1 );
        }

        public Cons ToCons( IEnumerable<object> e ) {
            Cons current = null;
            foreach( var elm in Enumerable.Reverse( e ) ) {
                current = new Cons( elm, current );
            }
            return current;
        }

        public IEnumerator<object> GetEnumerator() {
            return new ConsEnumerator( this );
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }

    public struct Keyword {
        public String Name {
            get;
            private set;
        }

        public Keyword( String s ) : this() {
            Name = String.Intern( s );
        }

        public override bool Equals( object other ) {
            var kw = other as Keyword?;
            return kw != null && Name == kw.Value.Name;
        }

        public static bool operator ==( Keyword a, Keyword b ) {
            return Object.ReferenceEquals( a.Name, b.Name );
        }

        public static bool operator !=( Keyword a, Keyword b ) {
            return !( a == b );
        }

        public override int GetHashCode() {
            return 0x110F9E0F ^ Name.GetHashCode();
        }
    }

    public struct Symbol {
        public String Name {
            get;
            private set;
        }

        public String Namespace {
            get;
            private set;
        }

        public Symbol( string ns, string s ) : this() {
            Name = String.Intern( s );
            Namespace = ns == null ? null : String.Intern( ns );
        }
        public static bool operator ==( Symbol a, Symbol b ) {
            return Object.ReferenceEquals( a.Name, b.Name )
                && Object.ReferenceEquals( a.Namespace, b.Namespace );
        }

        public static bool operator !=( Symbol a, Symbol b ) {
            return !( a == b );
        }

        public override bool Equals( object other ) {
            var sym = other as Symbol?;
            return sym != null
                && Name == sym.Value.Name
                && Namespace == sym.Value.Namespace;
        }

        public override int GetHashCode() {
            var code = 0x70F03A0B ^ Name.GetHashCode();
            if( Namespace != null ) code ^= Namespace.GetHashCode();
            return code;
        }

        public override string ToString() {
            if( Namespace != null ) {
                return Namespace + "::" + Name;
            } else {
                return Name;
            }
        }

        public string ToCSharpQualifiedName() {
            if( Namespace != null ) {
                return Namespace.Replace( "::", "." ) + "." + Name;
            } else {
                return Name;
            }
        }
    }

    public class SyntaxError : Exception {
        public SyntaxError( string msg ) : base( msg ) { }
    }


    public class Lisp {
        public void Start() {
            var parser = new Parser( "<unknown>", "(wow ())" );
            var r = parser.ParseAny();
        }

        public static string ShowLisp( object v ) {
            if( v == null ) return "null";
            if( v.Equals( true ) ) return "true";
            if( v.Equals( false ) ) return "false";

            var inum = v as int?;
            if( inum != null )
                return inum.Value.ToString( System.Globalization.CultureInfo.InvariantCulture );

            var fnum = v as float?;
            if( fnum != null ) {
                var fstr =
                    fnum.Value.ToString( System.Globalization.CultureInfo.InvariantCulture );

                if( !( fstr.Contains( 'e' ) || fstr.Contains( 'E' ) || fstr.Contains( '.' ) ) )
                    fstr += ".0";
                return fstr;
            }

            var str = v as String;
            if( str != null ) return StringRep( str );

            var sym = v as Symbol?;
            if( sym != null ) return sym.Value.ToString();

            var kw = v as Keyword?;
            if( kw != null ) return ":" + kw.Value.Name;

            var sexp = v as Cons;
            if( sexp != null ) {
                if( Constants.SYM_UNQUOTE.Equals( sexp.Head )
                 && sexp.Tail != null ) {
                    return "'" + ShowLisp( sexp.Tail.Head );
                }
                var r = "(";
                bool first = true;
                foreach( var elm in sexp ) {
                    if( !first ) r += " ";
                    r += ShowLisp( elm );
                    first = false;
                }
                r += ")";
                return r;
            }

            var arr = v as List<object>;
            if( arr != null ) {
                var r = "[";
                var count = arr.Count;
                foreach( var elm in arr ) {
                    count--;
                    r += ShowLisp( elm );
                    if( count > 0 ) r += " ";
                }
                r += "]";
                return r;
            }

            var dict = v as Dictionary<object, object>;
            if( dict != null ) {
                var r = "{";
                var count = dict.Count;
                foreach( var elm in dict ) {
                    count--;
                    r += ShowLisp( elm.Key );
                    r += " => ";
                    r += ShowLisp( elm.Key );
                    if( count > 0 ) r += " ";
                }
                r += "}";
                return r;
            }

            return "<" + v.ToString() + ">";
        }

        public static string StringRep( string str ) {
            return "\"" + str.Replace( "\\", "\\\\" )
                             .Replace( "\"", "\\\"" )
                             .Replace( "\n", "\\n" ) + "\"";
        }

        public static void Main() {
            Repl();
        }

        public static void Repl() {
            var interpreter = new Interpreter();
            while( true ) {
                Console.Write( ">> " );

                var inp = "";
                var parenBalance = 0;
                var bracketBalance = 0;
                var braceBalance = 0;
                do {
                    var l = Console.ReadLine();
                    if( l == null ) {
                        return;
                    }
                    inp += l;
                    parenBalance = inp.Count( x => x == '(' ) - inp.Count( x => x == ')' );
                    bracketBalance = inp.Count( x => x == '[' ) - inp.Count( x => x == ']' );
                    braceBalance = inp.Count( x => x == '{' ) - inp.Count( x => x == '}' );
                    if( parenBalance > 0 || braceBalance > 0 || bracketBalance > 0 ) {
                        Console.Write( "   " );
                    }
                } while( parenBalance > 0 || braceBalance > 0 || bracketBalance > 0 );
                var parser = new Parser( "<repl>", inp );
                foreach( var e in parser.ParseAll() ) {
                    try {
                        Console.WriteLine( ShowLisp( interpreter.Do( e ) ) );
                    } catch( Exception err ) {
                        Console.WriteLine( err.Message );
                        Console.WriteLine( interpreter.StackTrace() );
                        Console.WriteLine( "=== FULL STACK TRACE ===" );
                        Console.WriteLine( err );
                    }
                }
                if( parser.Failed ) {
                    Console.WriteLine( parser.ErrorMessage() );
                }
            }
        }
    }
}