﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Lys {
    public struct SourceLoc {
        public string FileName;
        public int Line;
        public int Pos;
    }

    public class Token {
        public object Value {
            get;
            private set;
        }

        public SourceLoc Loc {
            get;
            private set;
        }

        public Token( object val, SourceLoc loc ) {
            Loc = loc;
            Value = val;
        }
    }

    public class Parser {
        string src;
        int index;

        public String FileName {
            get;
            private set;
        }

        public int Line {
            get;
            private set;
        }

        public int Pos {
            get;
            private set;
        }

        public bool Failed {
            get { return Error != null; }
        }

        public string Error {
            get;
            private set;
        }

        char Cur {
            get { return src[index]; }
        }

        bool Eof {
            get { return index >= src.Length; }
        }

        SourceLoc markedLoc;

        public Parser( string fileName, string src, bool skipIgnore = true ) {
            FileName = fileName;
            this.src = src;
            index = 0;
            Line = 1;
            Pos = 1;
            if( skipIgnore ) SkipIgnore();
        }

        public static Token Parse( string fileName, string source ) {
            return new Parser( fileName, source ).ParseAny();
        }

        public static IEnumerable<Token> ParseAll( string fileName, string source ) {
            return new Parser( fileName, source ).ParseAll();
        }

        void MarkLocation() {
            markedLoc = new SourceLoc {
                FileName = FileName,
                Line = Line,
                Pos = Pos
            };
        }

        Token EmptyCurrentLocToken() {
            return new Token( null, markedLoc );
        }

        Token MakeToken( object value ) {
            return new Token( value, markedLoc );
        }

        Token MakeTokenHere( object value ) {
            MarkLocation();
            return MakeToken( value );
        }

        public void SkipIgnore() {
            while( !Eof ) {
                if( IsIgnore() ) {
                    Next();
                    continue;
                }

                if( !Eof && IsComment() ) {
                    do {
                        if( Next() ) break;
                    } while( Cur != '\n' );
                }
                break;
            }
        }

        public bool Peek( out char c ) {
            c = '\x00';
            if( ( index + 1 ) >= src.Length ) return false;
            c = src[index + 1];
            return true;
        }

        public bool IsIgnore() {
            if( Eof ) return false;
            return Cur == ' ' || Cur == '\t' || Cur == '\n';
        }

        public bool IsComment() {
            if( Eof ) return false;
            char peek;
            if( Cur == ';' && Peek( out peek ) ) return peek == ';';
            else return false;
        }

        public bool IsSpecial() {
            if( Eof ) return false;
            return Cur == '('
                || Cur == ')'
                || Cur == '['
                || Cur == ']'
                || Cur == '{'
                || Cur == '}'
                || Cur == '"'
                || Cur == '\''
                || Cur == ','
                || Cur == '`'
                || Cur == ':'
                || Cur == ';'
                || Cur == '.'
                || Cur == '/'
                || Cur == '$';
        }

        public bool IsNum() {
            if( Eof ) return false;
            return Cur >= '0' && Cur <= '9';
        }

        public bool Next() {
            index++;
            if( !Eof ) {
                if( Cur == '\n' ) {
                    Line++;
                    Pos = 1;
                } else {
                    Pos++;
                }
            }
            return Eof;
        }

        public IEnumerable<Token> ParseAll() {
            while( !Eof ) {
                Token r = null;

                try {
                    r = ParseAny();
                    SkipIgnore();
                } catch( SyntaxError _e ) {
                    yield break;
                }

                yield return r;
            }
        }

        public Token ParseAny() {
            char peek;
            switch( Cur ) {
            case '(':
                return ParseSexp();
            case '[':
                return ParseArray();
            case '{':
                return ParseDict();
            case '"':
                return ParseString();
            case ':':
                return ParseKeyword();
            case '\'':
                return ParsePrefixed( "quote" );
            case ',':
                if( Peek( out peek ) && peek == '@' ) {
                    Next();
                    return ParsePrefixed( "unquote-splice" );
                }
                return ParsePrefixed( "unquote" );
            case '`':
                return ParsePrefixed( "quasiquote" );
            case ')':
                return Fail( "Unmatched ')'" );
            case ']':
                return Fail( "Unmatched ']'" );
            case '}':
                return Fail( "Unmatched '}'" );
            case '/':
                MarkLocation();
                if( !Next() && !( IsComment() || IsSpecial() || IsIgnore() ) )
                    Fail( "Invalid symbol, '/' is not allowed outside static fields" );
                SkipIgnore();
                return MakeToken( new Symbol( null, "/" ) );
            case '-':
                if( Peek( out peek ) && peek >= '0' && peek <= '9' ) return ParseNumber();
                goto default;
            default:
                if( IsNum() ) return ParseNumber();
                else return ParseSymbol();
            }
        }

        // Marks
        public Token ParseSexp() {
            MarkLocation();
            if( Next() ) Fail( "Unmatched '(', got EOF" );
            SkipIgnore();

            var savePoint = markedLoc;
            Cons head = null;
            Cons current = null;

            while( !Eof && Cur != ')' ) {
                var elm = ParseAny();
                SkipIgnore();

                if( head == null ) current = head = new Cons( elm, null );
                else {
                    var last = new Cons( elm, null );
                    current.Tail = last;
                    current = last;
                }
            }
            if( Eof ) Fail( "Unmatched '('" );

            Next();
            return new Token( head, savePoint );
        }

        // Marks
        public Token ParseArray() {
            MarkLocation();
            if( Next() ) Fail( "Expected array elements, got EOF" );
            SkipIgnore();

            var savePoint = markedLoc;
            var arr = new List<object>();

            while( !Eof && Cur != ']' ) {
                arr.Add( ParseAny() );
                SkipIgnore();
            }
            if( Eof ) Fail( "Unmatched '['" );
            Next();
            SkipIgnore();

            return new Token( arr, savePoint );
        }

        // Marks
        public Token ParseDict() {
            MarkLocation();
            if( Next() ) Fail( "Expected dictionary elements, got EOF" );
            SkipIgnore();

            var savePoint = markedLoc;
            var dict = new Dictionary<object, object>();

            while( !Eof && Cur != '}' ) {
                var key = ParseAny();
                SkipIgnore();
                char peek;
                if( Eof || Cur != '=' || !Peek( out peek ) || peek != '>' )
                    Fail( "Expected '=>'" );

                Next();
                Next();
                SkipIgnore();
                if( Eof ) Fail( "Expected dictionary entry value, got EOF" );
                try {
                    dict.Add( key, ParseAny() );
                } catch( SyntaxError e ) {
                    if( Cur == '}' ) Fail( "Expected dictionary entry value" );
                    throw e;
                }
                SkipIgnore();
            }
            if( Eof ) Fail( "Unmatched '{'" );
            Next();
            SkipIgnore();

            return new Token( dict, savePoint );
        }

        // Marks
        public Token ParseString() {
            if( Next() ) Fail( "Expected closing '\"', got EOF" );

            MarkLocation();
            var buf = new StringBuilder();

            while( Cur != '"' && Cur != '\n' && Cur != '\r' ) {

                if( Cur == '\\' ) {
                    char peek;

                    if( Peek( out peek ) ) {
                        switch( peek ) {
                        case '\\':
                            buf.Append( '\\' );
                            break;
                        case 'n':
                            buf.Append( '\n' );
                            break;
                        case '"':
                            buf.Append( '"' );
                            break;
                        default:
                            Fail( "Invalid escape code" );
                            break;
                        }
                        Next();
                    } else Fail( "Invalid escape code, got EOF" );
                } else buf.Append( Cur );

                if( Next() ) Fail( "Expected closing '\"', got EOF" );
            }

            if( Cur != '"' ) Fail( "Expected closing '\"'" );
            Next();
            SkipIgnore();

            return MakeToken( buf.ToString() );
        }

        // Marks
        public Token ParseKeyword() {
            if( Next() ) Fail( "Expected keyword, got EOF" );

            if( IsSpecial() || IsIgnore() || IsComment() ) Fail( "Expected keyword" );

            return MakeTokenHere( new Keyword( ParseIdentifier() ) );
        }

        // Marks
        public Token ParsePrefixed( string prefix ) {
            if( Next() ) Fail( "Expected quoted expression, got EOF" );
            SkipIgnore();

            MarkLocation();
            var qe = new Cons( new Symbol( null, prefix )
                             , new Cons( ParseAny(), null ) );
            SkipIgnore();

            return MakeToken( qe );
        }


        // Doesn't mark
        public string ParseIdentifier() {
            if( Eof ) Fail( "Expected identifer, got EOF" );
            var s = new StringBuilder().Append( Cur );

            while( !( Next() || IsIgnore() || IsComment() || IsSpecial() ) ) {
                s.Append( Cur );
            }

            return s.ToString();
        }

        // Doesn't mark
        public object ParseSpecialIdentifier() {
            var ident = ParseIdentifier();
            if( ident == "null" ) return null;
            if( ident == "true" ) return true;
            if( ident == "false" ) return false;
            return ident;
        }

        // Marks
        public Token ParseSymbol() {
            if( Eof ) Fail( "Expected symbol got EOF" );

            MarkLocation();

            if( Cur == '.' ) {
                if( Next() || ((IsIgnore() || IsComment() || IsSpecial()) && Cur != '.'  ) )
                    return MakeToken( new Symbol( null, "." ) );

                if( Cur == '.' ) Fail( "Invalid syntax, expect field name got `.`" );

                var fragment = ParseSpecialIdentifier();
                var field = fragment as string;
                if( field == null )
                    Fail( "Invalid field, `true`, `false` and `null` are not allowed" );

                if( !Eof && Cur == '.' ) Fail( "Invalid syntax, chaining fields is not allowed" );

                return MakeToken( new Cons( new Symbol( null, "." )
                                          , new Cons( new Symbol( null, field ), null ) ) );
            }

            string name = null;
            string ns = null;

            while( true ) {
                var i = ParseSpecialIdentifier();
                var fragment = i as string;


                if( fragment == null ) {
                    if( (!Eof && Cur == ':') || name != null || ns != null )
                        Fail( "Invalid symbol, `true`, `false` and `null` are not allowed" );

                    return MakeToken( i );
                }

                if( name == null ) name = fragment;
                else if( ns == null ) {
                    ns = name;
                    name = fragment;
                } else {
                    ns += "::" + name;
                    name = fragment;
                }

                if( Eof || Cur != ':' ) break;
                Next();
                if( Eof || Cur != ':' )
                    Fail( "Invalid symbol, expected namespace delimiter `::`" );
                Next();
            }

            if( !Eof && (Cur == '.' || Cur == '/') ) {
                var isStatic = Cur == '/';

                if( Next() || IsIgnore() || IsComment() || IsSpecial() )
                    Fail( "Expected field" );

                var fragment = ParseSpecialIdentifier();
                var field = fragment as string;
                if( field == null )
                    Fail( "Invalid field, `true`, `false` and `null` are not allowed" );

                if( !Eof && Cur == '.' ) Fail( "Invalid syntax, chaining fields is not allowed" );

                Cons tail = null;
                if( isStatic )
                    tail = new Cons( new Keyword( "static" ), null );

                return MakeToken( new Cons( new Symbol( null, "." )
                                          , new Cons( new Symbol( ns, name )
                                                    , new Cons( new Symbol( null, field ), tail ) ) ) );
            }

            SkipIgnore();

            return MakeToken( new Symbol( ns, name ) );
        }

        // Marks
        public Token ParseNumber() {
            if( Eof ) Fail( "Expected number, got EOF" );

            MarkLocation();
            var isFloat = false;
            var num = new StringBuilder( "" + Cur );
            while( !Next() && IsNum() ) num.Append( Cur );
            if( !Eof && Cur == '.' ) {
                num.Append( '.' );
                isFloat = true;
                while( !Next() && IsNum() ) num.Append( Cur );
            }
            if( !Eof && ( Cur == 'e' || Cur == 'E' ) ) {
                bool firstDigit = true;
                num.Append( 'e' );
                isFloat = true;
                while( !Next() && ( IsNum() || Cur == '-' ) ) {
                    if( Cur == '-' ) {
                        if( !firstDigit ) Fail( "Invalid number" );
                        num.Append( '-' );
                    } else {
                        num.Append( Cur );
                    }
                    firstDigit = false;
                }
                if( firstDigit ) Fail( "Invalid number" );
            }
            if( !Eof && !( IsSpecial() || IsComment() || IsIgnore() ) ) Fail( "Invalid number" );
            SkipIgnore();

            if( isFloat ) {
                var r = Convert.ToSingle( num.ToString()
                                        , System.Globalization.CultureInfo.InvariantCulture );
                return MakeToken( r );
            } else {
                var r = Convert.ToInt32( num.ToString()
                                       , System.Globalization.CultureInfo.InvariantCulture );
                return MakeToken( r );
            }
        }

        public Token Fail( string error ) {
            Error = error;
            throw new SyntaxError( error );
        }

        public string ErrorMessage() {
            if( !Failed ) return null;
            var srcLine = src.Split( '\n' ).Skip( Line - 1 ).First();
            var arrow = new StringBuilder( "  " ).Append( '~', Pos - 1 ).Append( '^' );
            return $"Error: {Error} in {FileName} {Line}:{Pos}\n  {srcLine}\n{arrow}\n";
        }
    }
}

