﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Lys;
using NUnit.Framework;

namespace LysTests {
    class TestInterpreter {

        public class A { }
        public class B : A { }
        public class C : B { }

        public void M0( C a, A b, C c ) { }
        public void M1( C a, B b, C c ) { }

        public void N0( A a, B b, C c ) { }
        public void N1( C a, B b, A c ) { }

        public void Q( C a, A b = null ) { }

        [Test]
        public void TestResolveInvoke() {
            var t = typeof( TestInterpreter );
            var ms = new MethodBase[] {  t.GetMethod( "M0" ), t.GetMethod( "M1" ) };
            var c = typeof( C );

            Assert.That( AmbiguousMember.ResolveInvoke( ms, new Type[] { c, c, null } )
                       , Is.EqualTo( t.GetMethod( "M1" ) ) );

            Assert.Throws<InvalidArguments>( () => 
                AmbiguousMember.ResolveInvoke( ms, new Type[] { c, null, c } )  );

            var ns = new MethodBase[] { t.GetMethod( "N0" ), t.GetMethod( "N1" ) };

            Assert.Throws<InvalidArguments>( () =>
                AmbiguousMember.ResolveInvoke( ns, new Type[] { c, c, c } )  );

            var writeLines =
                Type.GetType( "System.Console" ).GetMethods().Where( x => x.Name == "WriteLine" );

            Assert.That(
                AmbiguousMember.ResolveInvoke( writeLines, new Type[] { typeof( string ) } )
               , Is.EqualTo( writeLines.Where( x => {
                   var prms = x.GetParameters();
                   return prms.Length > 0 && prms[0].ParameterType == typeof( string );
               } ).First() ) );

            Assert.That(
                AmbiguousMember.ResolveInvoke( writeLines, new Type[] { c } )
               , Is.EqualTo( writeLines.Where( x => {
                   var prms = x.GetParameters();
                   return prms.Length > 0 && prms[0].ParameterType == typeof( object );
               } ).First() ) );

            Assert.That(
                AmbiguousMember.ResolveInvoke( new MethodBase[] { t.GetMethod( "Q" ) }
                                             , new Type[] { c } )
              , Is.EqualTo( t.GetMethod( "Q" ) ) );

            Assert.That(
                AmbiguousMember.ResolveInvoke( new MethodBase[] { t.GetMethod( "Q" ) }
                                             , new Type[] { c, c } )
              , Is.EqualTo( t.GetMethod( "Q" ) ) );
        }


        public object Run( string src ) {
            var interpreter = new Interpreter();
            return interpreter.EvalEach( Parser.ParseAll( "<test>", src ) );
        }

        public void TestEvaluationResult( string src, object expected ) {
            var interpreter = new Interpreter();
            Assert.That( interpreter.EvalEach( Parser.ParseAll( "<test>", src ) )
                       , Is.EqualTo( expected ) );
        }

        // Test that atoms evaluate to atoms:
        [TestCase( "1", "1" )]
        [TestCase( "null", "null" )]
        [TestCase( "()", "null" )]
        [TestCase( "true", "true" )]
        [TestCase( "\"wow\"", "\"wow\"" )]
        [TestCase( ":kw", ":kw" )]
        public void TestEvaluationResult( string src, string expectedSrc ) {
            var interpreter = new Interpreter();
            Assert.That( interpreter.EvalEach( Parser.ParseAll( "<test>", src ) )
                       , Is.EqualTo( interpreter.EvalEach( Parser.ParseAll( "<test>", src ) ) ) );
        }

        [Test]
        public void TestInvokes() {
            Assert.Throws<UncallableValue>( () => Run( "(1 2 3)" ) );
            Assert.That( Run( "(print \"wow\")" ), Is.Null );
            Assert.That( Run( "()" ), Is.Null );
            Assert.That( Run( "(Lys::Cons null null)" ), Is.EqualTo( new Cons( null, null ) ) );
            Assert.That( Run( "(Lys::Cons 1 null)" ), Is.EqualTo( new Cons( 1, null ) ) );
            Assert.Throws<InvalidArguments>( () => Run( "(Lys::Cons 1)" ) );
            Assert.That( Run( "((. Head) '(1 2))" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "((. Lys::Cons Count) '(1 2))" )
                       , Is.EqualTo( Run( "((. Count) '(1 2))" ) ) );
            Assert.Throws<InvalidArguments>( () => Run( "(print 1)" ) );
            Assert.Throws<InvalidArguments>( () => Run( "((. Head) null)" ) );
            Assert.Throws<InvalidArguments>( () => Run( "((. Head))" ) );
            Assert.Throws<InvalidArguments>( () => Run( "((. Head) \"nope\")" ) );
            Assert.Throws<InvalidArguments>( () =>
                Run( "((. System::Console WriteLine :static) null)" ) );
            Assert.That( Run( "((. Lys::Parser Parse :static) \"<test>\" \"1\")" )
                       , Is.EqualTo( 1 ) );
        }

        [Test]
        public void TestDot() {
            Assert.That( Run( "((. Head) '(1 2))" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(using Lys) ((. Cons Head) '(1 2))" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "((. Lys::Cons Head) '(1 2))" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(. System::Console WriteLine :static)" )
                       , Is.TypeOf<AmbiguousStaticMember>() );

            Assert.Throws<InvalidForm>( () => Run( "(.)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(. a b :static d)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(. So Far :wow)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(. null Wow :static)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(. Wow Invalid::Wow :static)" ) );
        }

        [Test]
        public void TestDefun() {
            Assert.That( Run( "(defun test []) (test)" ), Is.Null );
            Assert.That( Run( "(defun test [x]) (test 1)" ), Is.Null );
            Assert.That( Run( "(defun test [x] x) (test 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(defun test [x y] x y) (test 1 2)" ), Is.EqualTo( 2 ) );

            Assert.Throws<InvalidForm>( () => Run( "(defun)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(defun test)" ) );
        }

        [Test]
        public void TestLet() {
            Assert.That( Run( "(let [])" ), Is.Null );
            Assert.That( Run( "(let [x 1] x)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(let [x 1 y 2] x y)" ), Is.EqualTo( 2 ) );
            Assert.That( Run( "(let [x 1 y x] y)" ), Is.EqualTo( 1 ) );

            Assert.Throws<UndefinedSymbol>( () => Run( "(let [x y y 1])" ) );
            Assert.Throws<UndefinedSymbol>( () => Run( "(let [x 1]) x" ) );
            Assert.Throws<InvalidForm>( () => Run( "(let)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(let [x 1 y])" ) );
        }

        [Test]
        public void TestIf() {
            Assert.That( Run( "(if true 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(if false 1)" ), Is.Null );
            Assert.That( Run( "(if false 1 2)" ), Is.EqualTo( 2 ) );
            Assert.That( Run( "(if true 1 2)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(if null 1 2)" ), Is.EqualTo( 2 ) );
            Assert.That( Run( "(if :wow 1 2)" ), Is.EqualTo( 1 ) );

            Assert.Throws<InvalidForm>( () => Run( "(if)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(if true)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(if true :wow :ser :err)" ) );
        }

        [Test]
        public void TestLambda() {
            Assert.That( Run( "((lambda []))" ), Is.Null );
            Assert.That( Run( "((lambda [x]) 1)" ), Is.Null );
            Assert.That( Run( "((lambda [x] x) 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "((lambda [x y] x y) 1 2)" ), Is.EqualTo( 2 ) );

            Assert.Throws<InvalidForm>( () => Run( "(lambda)" ) );
        }

        [Test]
        public void TestMacros() {
            Assert.That( Run( "(macrofun test []) (test)" ), Is.Null );
            Assert.That( Run( "(macrofun test [x]) (test 1)" ), Is.Null );
            Assert.That( Run( "(macrofun test [x] x) (test 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(macrofun test [x y] x y) (test 1 2)" ), Is.EqualTo( 2 ) );

            Assert.That( Run( "(quasiquote)" ), Is.Null );
            Assert.That( Run( "(macrofun test [x y] `'(x ,y)) (test 1 :wow)" )
                       , Is.EqualTo( new Cons( new Symbol( null, "x" )
                                             , new Cons( new Keyword( "wow" ), null ) ) ) );

            Assert.That( Run( "(macrofun test [x] `'(0 ,@x 3)) (test (1 2))" )
                       , Is.EqualTo( new Cons( 0
                                             , new Cons( 1
                                                       , new Cons( 2
                                                                 , new Cons( 3, null ) ) ) ) ) );

            Assert.Throws<InvalidForm>( () => Run( "(quasiquote 1 2)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(macrofun)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(macrofun wow)" ) );
            Assert.Throws<InvalidForm>( () => Run( ",x" ) );
            Assert.Throws<InvalidForm>( () => Run( ",@x" ) );
        }

        [Test]
        public void TestDo() {
            Assert.That( Run( "(do)"), Is.Null );
            Assert.That( Run( "(do 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(do 1 2)" ), Is.EqualTo( 2 ) );
            Assert.That( Run( "(do 1 ((. Head) '(2 1)))" ), Is.EqualTo( 2 ) );
        }

        [Test]
        public void TestUsing() {
            Assert.That( Run( "(using Lys) Cons" ), Is.EqualTo( typeof( Cons ) ) );
            Assert.That( Run( "(using Lys System::Text) StringBuilder" )
                       , Is.EqualTo( typeof( StringBuilder ) ) );
            Assert.That( Run( "(using System::Linq::Expressions) Expression" )
                       , Is.EqualTo( typeof( System.Linq.Expressions.Expression ) ) );

            Assert.Throws<InvalidForm>( () => Run( "(using)" ) );
            Assert.Throws<Exception>( () => Run( "(using asd)" ) );
        }

        [Test]
        public void TestForeach() {
            Assert.That( Run( "(foreach x '(1 2 3) x)" ), Is.EqualTo( 3 ) );
            Assert.That( Run( "(foreach x '() x)" ), Is.Null );
            Assert.That( Run( "(foreach x '())" ), Is.Null );
            Assert.That( Run( "(foreach x [(1 2) (2 3) (3 4)] ((. Head) x))" ), Is.EqualTo( 3 ) );

            Assert.Throws<InvalidForm>( () => Run( "(foreach)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(foreach x)" ) );
        }

        [Test]
        public void TestTry() {
            Assert.That( Run( "(try 1)" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(try 1 (catch System::Exception e 2))" ), Is.EqualTo( 1 ) );
            Assert.That( Run( "(try 1 (finally 2))" ), Is.EqualTo( 2 ) );
            Assert.That( Run( "(try 1 (finally 2 3))" ), Is.EqualTo( 3 ) );
            Console.WriteLine( Parser.Parse( "<test>", "(try (idiv 1 0) (catch System::Exception e 1))" ) );
            Assert.That( Run( "(try (idiv 1 0) (catch System::Exception e 1))" ), Is.EqualTo( 1 ) );
            Assert.That(
                Run( "(try (idiv 1 0) (catch System::Exception e 1) (catch Lys::InvalidForm e 2))" )
                , Is.EqualTo( 1 ) );
            Assert.That(
                Run( "(try (idiv 1 0) (catch Lys::InvalidForm e 2) (catch System::Exception e 1))" )
                , Is.EqualTo( 1 ) );
            Assert.That( Run( "(try (idiv 1 0) (catch System::Exception e 1) (finally 2))" )
                       , Is.EqualTo( 2 ) );

            Assert.That( Run( "(try (throw (System::Exception \"test\")) " +
                              "(catch System::Exception e (.Message e)))" )
                       , Is.EqualTo( "test" ) );

            Assert.Throws<InvalidForm>( () => Run( "(try)" ) );
            Assert.Throws<TypeError>( () => Run( "(try x x)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(try x (catch x))" ) );
            Assert.Throws<InvalidForm>( () => Run( "(try x (wrong))" ) );
            Assert.Throws<DivideByZeroException>( () =>
                Run( "(try (idiv 1 0) (catch Lys::InvalidForm e))" ) );
        }
    
        [Test]
        public void TestThrow() {
            Assert.Throws<InvalidOperationException>( () =>
                Run( "(throw (System::InvalidOperationException))" ) );

            Assert.Throws<TypeError>( () => Run( "(throw 1)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(throw)" ) );
            Assert.Throws<InvalidForm>( () => Run( "(throw a b)" ) );
        }
    }
}
