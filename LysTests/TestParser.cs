﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Lys;

namespace LysTests {

    public class TestParser {
        [TestCase( "  ", false )]
        [TestCase( "", false )]
        [TestCase( "wow", false )]
        [TestCase( "; not a comment", false )]
        [TestCase( " ;; not", false )]
        [TestCase( ";; yes", true )]
        public void TestIsComment( String src, bool isIt ) {
            Assert.That( Is.Equals( new Parser( "<test>", src, false ).IsComment(), isIt ) );
        }

        [TestCase( "", false )]
        [TestCase( "  ", true )]
        [TestCase( "wow", false )]
        [TestCase( ";; comment\na", false )]
        [TestCase( "   ;;commnent", true )]
        public void TestIsIgnore( String src, bool isIt ) {
            Assert.That( Is.Equals( new Parser( "<test>", src, false ).IsIgnore(), isIt ) );
        }

        [TestCase( "", false )]
        [TestCase( "  ", false )]
        [TestCase( ";; wow", false )]
        [TestCase( "a", false )]
        [TestCase( "13", true )]
        public void TestIsNum( String src, bool isIt ) {
            Assert.That( Is.Equals( new Parser( "<test>", src, false ).IsNum(), isIt ) );
        }

        [TestCase( "", false )]
        [TestCase( " ", false )]
        [TestCase( ";; wow", true )]
        [TestCase( "a", false )]
        [TestCase( "()", true )]
        [TestCase( "a(", false )]
        [TestCase( "\"", true )]
        public void TestIsSpecial( String src, bool isIt ) {
            Assert.That( Is.Equals( new Parser( "<test>", src, false ).IsSpecial(), isIt ) );
        }

        [TestCase( "", 1 )]
        [TestCase( "a", 1 )]
        [TestCase( ";; wow", 6 )] // Pos stops at the last character
        [TestCase( "  a", 3 )]
        [TestCase( "   ", 3 )]
        public void TestSkipIgnore( String src, int pos ) {
            var p = new Parser( "<test>", src );
            Assert.That( Is.Equals( p.Pos, pos ) );
        }

        // Assumes the initial char is a valid start to the identifier
        [TestCase( "", "", true )]
        [TestCase( "a", "a" )]
        [TestCase( "123", "123" )]
        [TestCase( "a ", "a" )]
        [TestCase( "wow/ser.dot", "wow" )]
        [TestCase( "a()", "a" )]
        public void TestParseIdentifier( string src, object expect, bool shouldThrow = false ) {
            TestAParser( "ParseIdentifier", src, expect, shouldThrow );
        }

        object UnwrapToken( object obj, List<SourceLoc> locs = null ) {
            var tk = obj as Token;
            if( tk != null ) {
                if( locs != null )
                    locs.Add( tk.Loc );
                obj = tk.Value;
            }
            var cons = obj as Cons;
            if( cons != null ) {
                var head = cons.Head as Token;
                if( head != null )
                    return new Cons( UnwrapToken( head ), (Cons) UnwrapToken( cons.Tail ) );
                return new Cons( cons.Head, (Cons) UnwrapToken( cons.Tail ) );
            }

            var arr = obj as List<object>;
            if( arr != null ) {
                return arr.Select( elm => {
                    var etk = elm as Token;
                    if( etk != null ) {
                        return UnwrapToken( etk );
                    }
                    return elm;
                } ).ToList();
            }

            var dict = obj as Dictionary<object, object>;

            if( dict != null ) {
                return dict.Select( kv => {
                    return new KeyValuePair<object, object>( UnwrapToken( kv.Key )
                                                           , UnwrapToken( kv.Value ) );
                } ).ToDictionary( kv => kv.Key, kv => kv.Value );
            }

            return obj;
        }

        public void TestAParser<T>( String meth, String src, T expect, bool shouldThrow = false
                                  , bool isCollection = false, object[] parserArgs = null ) {
            var parser = new Parser( "<test>", src, false );
            // TODO: Clean up this mess:
            TestDelegate testIt = () => {
                try {
                    var result =
                        UnwrapToken( parser.GetType()
                                           .GetMethod( meth )
                                           .Invoke( parser, parserArgs == null ? new object[] { }
                                                                               : parserArgs ) );
                    var t = expect as Type;
                    if( isCollection ) {
                        Assert.That( result
                                   , Is.EquivalentTo( (System.Collections.IEnumerable) expect ) );
                    } else if( t != null ) {
                        Assert.That( result, Is.TypeOf( t ) );
                    } else {
                        Assert.That( result, Is.EqualTo( expect ) );
                    }
                } catch( System.Reflection.TargetInvocationException e ) {
                    throw e.InnerException;
                }
            };
            if( shouldThrow ) Assert.Throws<SyntaxError>( testIt );
            else testIt();
        }

        [TestCase( "", "", true )]
        [TestCase( ":(", "", true )]
        [TestCase( ":", "", true )]
        [TestCase( ":wow", "wow", false )]
        public void TestParseKeyword( string src, string expect, bool shouldThrow = false ) {
            TestAParser( "ParseKeyword", src, new Keyword( expect ), shouldThrow );
        }

        [Test]
        public void TestParseSpecialIdentifier() {
            TestAParser( "ParseSpecialIdentifier", "wow", "wow" );
            TestAParser( "ParseSpecialIdentifier", "true", true );
            TestAParser( "ParseSpecialIdentifier", "false", false );
            TestAParser<object>( "ParseSpecialIdentifier", "null", null );
        }

        [Test]
        public void TestParseSymbol() {
            TestAParser( "ParseSymbol", "wow", new Symbol( null, "wow" ) );
            TestAParser( "ParseSymbol", "a::b", new Symbol( "a", "b" ) );
            TestAParser( "ParseSymbol", "a::b::c", new Symbol( "a::b", "c" ) );

            TestAParser( "ParseSymbol", ".a"
                       , new Cons( new Symbol( null, "." )
                                 , new Cons( new Symbol( null, "a" ), null ) ) );

            TestAParser( "ParseSymbol", "a.b"
                       , new Cons( new Symbol( null, "." )
                                 , new Cons( new Symbol( null, "a" )
                                           , new Cons( new Symbol( null, "b" ), null ) ) ) );

            TestAParser( "ParseSymbol", "a/b"
                       , new Cons( new Symbol( null, "." )
                                 , new Cons( new Symbol( null, "a" )
                                           , new Cons( new Symbol( null, "b" )
                                                     , new Cons( new Keyword( "static" )
                                                                , null ) ) ) ) );

            TestAParser( "ParseSymbol", ".a.", "", true );
            TestAParser( "ParseSymbol", "a.b.", "", true );
            TestAParser( "ParseSymbol", "..", "", true );
            TestAParser( "ParseSymbol", "a..", "", true );
            TestAParser( "ParseSymbol", "a/.", "", true );

            TestAParser( "ParseSymbol", "a:b::c", "", true );
            TestAParser( "ParseSymbol", "true::b::c", "", true );
            TestAParser( "ParseSymbol", "a::false::c", "", true );
            TestAParser( "ParseSymbol", "a::b::null", "", true );
        }

        [TestCase( "'wow", "quote", "wow" )]
        [TestCase( ",wow", "unquote", "wow" )]
        [TestCase( "`wow", "quasiquote", "wow" )]
        [TestCase( "'", "", "", true )]
        public void TestParseQuotes( string src, string prefix, string expect
                                   , bool shouldThrow = false ) {
            var sexp = new Cons( new Symbol( null, prefix )
                               , new Cons( new Symbol( null, expect ), null ) );
            TestAParser( "ParsePrefixed", src, sexp, shouldThrow
                       , parserArgs: new object[] { prefix } );
        }

        [Test]
        public void TestMultiQuote() {
            var q = new Symbol( null, "quote" );
            var inner = new Cons( q, new Cons( new Symbol( null, "wow" ), null ) );
            var outer = new Cons( q, new Cons( inner, null ) );
            TestAParser( "ParsePrefixed", "''wow", outer, false
                       , parserArgs: new object[] { "quote" } );
        }

        [TestCase( "", 0, true )]
        [TestCase( "0", 0 )]
        [TestCase( "999", 999 )]
        [TestCase( "-13", -13 )]
        [TestCase( "13.13", 13.13f )]
        [TestCase( "-13.37", -13.37f )]
        [TestCase( "10e5", 10e5f )]
        [TestCase( "-10e5", -10e5f )]
        [TestCase( "5e-1", 0.5f )]
        [TestCase( "80000e-4", 8f )]
        [TestCase( "1WOW", 0, true )]
        [TestCase( "10e--", 0, true )]
        [TestCase( "10e-wow", 0, true )]
        [TestCase( "10e", 0, true )]
        public void TestParseNumber( string src, object expected, bool shouldThrow = false ) {
            TestAParser( "ParseNumber", src, expected, shouldThrow );
        }

        // Assumes the first character is a double quote
        [TestCase( "\"", "", true )]
        [TestCase( "\"w\"", "w" )]
        [TestCase( "\"wow\"", "wow" )]
        [TestCase( "\"w\n", "", true )]
        [TestCase( "\"w\\", "w", true )]
        [TestCase( "\"\\n\\\"\\\\\"", "\n\"\\" )]
        [TestCase( "\"\\w\"", "", true )]
        public void TestParseString( string src, string expected, bool shouldThrow = false ) {
            TestAParser( "ParseString", src, expected, shouldThrow );
        }

        // Assumes the first character is an open bracket
        [TestCase( "[", null, true )]
        [TestCase( "[]", new int[] { } )]
        [TestCase( "[  ]", new int[] { } )]
        [TestCase( "[ 1 ]", new int[] { 1 } )]
        [TestCase( "[1 3 ]", new int[] { 1, 3 } )]
        [TestCase( "[1 3 ", null, true )]
        public void TestParseArray( string src, int[] expected, bool shouldThrow = false ) {
            List<int> l = null;
            if( expected != null ) l = new List<int>( expected );

            TestAParser( "ParseArray", src, l, shouldThrow );
        }

        // Assumes the first character is an open brace
        [TestCase( "{", null, null, true )]
        [TestCase( "{}", new object[] { }, new object[] { } )]
        [TestCase( "{  }", new object[] { }, new object[] { } )]
        [TestCase( "{ 10 => 11 }", new object[] { 10 }, new object[] { 11 } )]
        [TestCase( "{ 10 => 11 \n 11 => 12 }", new object[] { 10, 11 }, new object[] { 11, 12 } )]
        [TestCase( "{ 10 => }", null, null, true )]
        [TestCase( "{ 10 }", null, null, true )]
        [TestCase( "{ 10 => 11 ", null, null, true )]
        [TestCase( "{ 10 => ", null, null, true )]
        [TestCase( "{ 10 ", null, null, true )]
        public void TestParseDict( string src, object[] keys, object[] values
                                 , bool shouldThrow = false ) {
            Dictionary<object, object> d = null;
            if( keys != null && values != null ) {
                d = new Dictionary<object, object>();
                for( int i = 0; i < keys.Length; i++ ) {
                    d[keys[i]] = values[i];
                }
            }

            TestAParser( "ParseDict", src, d, shouldThrow );
        }

        [Test]
        public void TestParseSexp() {
            Cons emptySexp = null;
            var singleSexp = new Cons( 1, null );
            var multiSexp = new Cons( 1, new Cons( 2, new Cons( 3, null ) ) );
            var nestedSexp = new Cons( singleSexp, new Cons( multiSexp, null ) );

            TestAParser( "ParseSexp", "(", emptySexp, true );
            TestAParser( "ParseSexp", "()", emptySexp );
            TestAParser( "ParseSexp", "(1 )", singleSexp );
            TestAParser( "ParseSexp", "( 1 2 3)", multiSexp );
            TestAParser( "ParseSexp", "((1 ) (1 2 3))", nestedSexp );
            TestAParser( "ParseSexp", "(1 ", emptySexp, true );
            TestAParser( "ParseSexp", "( 1 2 3", emptySexp, true );
            TestAParser( "ParseSexp", "((1 ) (1 2 3)", emptySexp, true );
        }

        [Test]
        public void TestParseAny() {
            TestAParser( "ParseAny", "(sexp)", typeof( Cons ) );
            TestAParser( "ParseAny", "[ array ]", typeof( List<object> ) );
            TestAParser( "ParseAny", "{ hash => map }", typeof( Dictionary<object, object> ) );
            TestAParser( "ParseAny", ":keyword", typeof( Keyword ) );
            TestAParser( "ParseAny", "\'quote", typeof( Cons ) );
            TestAParser( "ParseAny", ")", typeof( int ), true );
            TestAParser( "ParseAny", "]", typeof( int ), true );
            TestAParser( "ParseAny", "}", typeof( int ), true );
            TestAParser( "ParseAny", "-10", typeof( int ) );
            TestAParser( "ParseAny", "-a", typeof( Symbol ) );
            TestAParser( "ParseAny", "10", typeof( int ) );
            TestAParser( "ParseAny", "wow!", typeof( Symbol ) );
            TestAParser( "ParseAny", "/", typeof( Symbol ) );
            TestAParser( "ParseAny", ".", typeof( Symbol ) );
            TestAParser( "ParseAny", ",@wow"
                       , new Cons( new Symbol( null, "unquote-splice" )
                                             , new Cons( new Symbol( null, "wow" ), null ) ) );
        }

        [TestCase( "", new int[] { } )]
        [TestCase( "  ", new int[] { } )]
        [TestCase( " 1 ", new int[] { 1 } )]
        [TestCase( " 2 1", new int[] { 2, 1 } )]
        public void TestParseAll( string src, int[] expected ) {
            var parser = new Parser( "<test>", src );
            var e = expected.Zip( parser.ParseAll(), ( x, y ) => new Tuple<int, Token>( x, y ) );
            foreach( var pair in e ) {
                Assert.That( pair.Item1, Is.EqualTo( pair.Item2.Value ) );
            }
        }

        [TestCase( "1", new int[] { 1 } )]
        [TestCase( "(  1 21   \"4\")", new int[] { 1, 4, 6, 11 } )]
        [TestCase( "[wow :ser {}]", new int[] { 1, 2, 6, 11 } )]
        [TestCase( "{ hello => there cool => syntax }", new int[] { 1, 3, 12, 18, 26 } )]
        public void TestSourceLocs( string src, int[] expectedPosses ) {
            var parser = new Parser( "<test>", src );
            var actualPosses = new List<SourceLoc>();
            UnwrapToken( parser.ParseAny(), actualPosses );
            var e = expectedPosses.Zip( actualPosses.Select( loc => loc.Pos ), (x, y) => new Tuple<int, int>( x, y ) );
            foreach( var pair in e ) {
                Assert.That( pair.Item1, Is.EqualTo( pair.Item2 ) );
            }
        }
    }
}
