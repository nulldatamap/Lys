# The Lys Programming Language
Lys is a lisp-like programming language for the Common Language Runtime and .NET framework.
It aims to be a both interpreted  and compiled. With a focus on
programmability, practicality, interop with C# and productivity. 

## Language overview
__Warning, everything is subject to change__
```clojure
;; Commnets
;; Numbers
10
-23
13.50
"strings"
;; Booleans and null
true false null
;; Arrays, lists and dictionaries
[1 2 3]
'(1 2 3)
{ :cat => "feline"
  :dog => "canine" }
;; Expressions
(+ 1 1) ;; => 2
(print "Hello world!")
;; Function definition
(defun add-one [x]
    (+ x 1))

(add-one 3) ;; => 4

;; Let bindings, lambdas, dos and if expressions!
(let [double (lambda [x] (+ x x))
      n      10]
    (if (= (double n) 20)
        (do (print "2x 10 is 20!")
            :yes)
        :no)) ;; => :yes

;; Macros
(macrofun unless [cond body]
    `(if (not ,cond) ,body))

(unless false
    :wow) ;; =expands to> (if (not false) :wow) => :wow

;; C# interop:
(using System::Text)
(let [sb (StringBuilder)]
    ;; Instance methods
    (.Append sb "Hello world!")
    ;; Static methods
    (System::Console/WriteLine (.ToString sb)))

;; Exception handling
(using System)
(try
    (throw (Exception "PANIC!"))
    (catch Exception e
        (print "It's fine. It was just: ")
        (print (.Message e)))
    (finally
       (print "at the disco.")))

```
More information will be supplied later.

This project is in its very early days of both design and implementation.
Contributions and discussion are welcome. 
